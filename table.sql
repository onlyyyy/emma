drop table conv;
-- auto-generated definition

create table conv
(
    id       INTEGER
        constraint conv_pk
            primary key autoincrement,
    sentence BLOB not null
);

create unique index conv_id_uindex
    on conv (id);

CREATE TABLE [word]([word] CHAR(4) NOT NULL ON CONFLICT FAIL UNIQUE ON CONFLICT IGNORE);

vacuum ;