db_path = './db'
fanzxl_db_path = './db/fanzxl.sqlite'
fk24_db_path = './db/fk24.sqlite'
haosys_db_path = './db/haosys.sqlite'
juemds_db_path = './db/juemds.sqlite'
laoyj_db_path = './db/laoyj.sqlite'
lost_db_path = './db/lost.sqlite'
prisionb_db_path = './db/prisionb.sqlite'
xiaohuangji_db_path = './db/xiaohuangji.sqlite'
dictionary_path = './db/dictionary.sqlite'

# 神经网络部分
BATCH_SIZE = 32
# INPUT_DIM =
# OUTPUT_DIM =
ENC_EMB_DIM = 256
DEC_EMB_DIM = 256
ENC_HID_DIM = 512
DEC_HID_DIM = 512
ENC_DROPOUT = 0.5
DEC_DROPOUT = 0.5
EPOCH = 50

# 每句话长度
DATA_LENGTH = 64
LONG_LIMIT = 30

# 神经网络的地址 只需要保存seq2seq的参数值就行了
attention_model = './models/attention.model'
encoder_model = './models/encoder.model'
decoder_model = './models/decoder.model'
seq2seq_model = './models/seq2seq.model'

# 日志的地址
log_path = './models/train.log'

# 生成的字典位置
token_path = './models/token.json'

