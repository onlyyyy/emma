import config
import lib.dbop
import jieba
import os
from lib.data_center import DataCenter

def bless():
    print(" _______  _______  _______  _______\n" +
          "(  ____ \(       )(       )(  ___  )\n" +
          "| (    \/| () () || () () || (   ) |\n" +
          "| (__    | || || || || || || (___) |\n" +
          "|  __)   | |(_)| || |(_)| ||  ___  |\n" +
          "| (      | |   | || |   | || (   ) |\n" +
          "| (____/\| )   ( || )   ( || )   ( |\n" +
          "(_______/|/     \||/     \||/     \|\n")


def main():
    if __name__ == '__main__':
        bless()
        # filetype = input("conv or other\n")
        filetype = 'conv'
        dataPath = './dataset/' + filetype
        files = os.listdir(dataPath)
        selected = input(str(files) + "选择要解析的文件，输入数字 从1开始\n")
        needed_filename = files[int(selected) - 1]
        file_name_split = needed_filename.split('.')[0]
        db_path = config.db_path +'/'+ file_name_split + '.sqlite'
        print("数据库地址为 ",db_path)
        dataset = DataCenter(db_path)

        print(needed_filename)
        dataset.load_conv('./dataset/conv/'+needed_filename)


main()
