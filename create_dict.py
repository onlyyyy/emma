# 用来创建字典
import config
import os
from lib.data_center import DataCenter


def main():
    filetype = 'dictionary'
    dataPath = './dataset/' + filetype
    files = os.listdir(dataPath)
    selected = input(str(files) + "选择要解析的文件，输入数字 从1开始\n")
    needed_filename = files[int(selected) - 1]
    db_path = config.dictionary_path
    print("数据库地址为 ", db_path)
    dataset = MyDataSet(db_path)

    dataset.gen_dict('./dataset/dictionary/' + needed_filename)


main()
